"""
.. module:: core.serializers
   :synopsis: This file contains `BaseModelSerializer` to be inherited by other serializers.

.. moduleauthor:: Mahesh


"""
from rest_framework import serializers


class BaseModelSerializer(serializers.ModelSerializer):
    """
    BaseModelSerializer
        Base class that extends default functionality of model serializer to be used by
        all serializers used for various data operations.

        Inherits : 'serializers.ModelSerializer'
    """

    def to_internal_value(self, data):
        """
        Check if data is provided in payload, if not, initialize data with empty dict.
        :param data:
        :return data:
        """
        if not data:
            data = {}
        return super().to_internal_value(data)
