"""
.. module:: core.mixins
   :synopsis: This file contains various mixins to be used in project to
    perform different operations.

.. moduleauthor:: Mahesh


"""


class ActionSerializerMixin(object):
    """
    ActionSerializerMixin
        Mixin to get respective serializer class from `action_serializer` dictionary
        based on action.
    """

    # Dictionary to store Serializer classes with respect to action
    action_serializers = {}

    def get_serializer_class(self):
        """
        get_serializer_class
            Fetch respective serializer based on action.
        """
        action = self.action.lower()

        if action in self.action_serializers:
            self.serializer_class = self.action_serializers.get(action)
        return super().get_serializer_class()
