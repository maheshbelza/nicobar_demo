from django.urls import reverse


def get_user_details_url(url_params):
    url = reverse('user-details', args=url_params)
    return url


def create_users_url(url_params):
    url = reverse('create-users', args=url_params)
    return url
