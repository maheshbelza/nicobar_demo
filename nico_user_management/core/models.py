"""
.. module:: core.models
   :synopsis: This file contains `BaseModel` to be inherited by other models
   along with model utility.

.. moduleauthor:: Mahesh


"""

import uuid

from django.db import models


def string_uuid():
    """
    string_uuid
        :param None:
        :return String of UUID4:
    """
    return str(uuid.uuid4())


class BaseModel(models.Model):
    """
        BaseModel
        This represents the BaseModel for the project to be used for creating any
        other model.
        Inherits : `models.Model`

    """
    """ Project level variables """
    id = models.CharField(primary_key=True, default=string_uuid, editable=False, max_length=36)
    deleted = models.DateTimeField(db_index=True, null=True, blank=True)
    last_modified_at = models.DateTimeField(auto_now=True, verbose_name='Last Updated Date')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Created Date')

    class Meta:
        abstract = True
