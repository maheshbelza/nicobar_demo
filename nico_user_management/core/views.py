"""
.. module:: core.views
   :synopsis: This file contains `BaseModelViewset` to be inherited by other viewsets
   to handle url requests.

.. moduleauthor:: Mahesh


"""
from rest_framework import permissions, viewsets

from core.mixins import ActionSerializerMixin


class BaseModelViewset(ActionSerializerMixin, viewsets.ModelViewSet):
    """
    BaseModelViewset
        Viewset to handle CRUD operations on a BaseModel instance.
        Handles: GET,PUT,POST request
        Inherits: `ActionSerializerMixin`, `viewsets.ModelViewSet`
    """

    permission_classes = ()
    model = None
    base_auth_permissions = (
        permissions.IsAuthenticated,
    )
    http_method_names = ['get', 'post', 'put', 'delete']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert self.model is not None, (
            "Attribute 'model' is not defined in {0}.".format(self.__class__.__name__)
        )

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permissions_list = self.base_auth_permissions + self.permission_classes
        return [permission() for permission in permissions_list]

    def get_queryset(self):
        """
        Return all records
        :return queryset:
        """
        queryset = self.model.objects.all()
        return self.apply_queryset_filters(queryset)

    def apply_queryset_filters(self, queryset):
        """
        Override if applying more complex filters to queryset.
        :param queryset:
        :return queryset:
        """
        return queryset
