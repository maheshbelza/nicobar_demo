# Installation Pre-requisite

* Install [Python 3.6.0] (https://www.python.org/downloads/release/python-360/)  and set PATH
```
$ wget https://www.python.org/ftp/python/3.6.0/Python-3.6.0.tar.xz
$ tar xJf Python-3.6.0.tar.xz
$ cd Python-3.6.0
$ ./configure
$ make
$ make install
```
* Install `virtualenv` tool, which is used to create isolated Python environments
* Create a mysql database named `nicobar` (Default user : `root`)

```
$ pip install virtualenv
```


# Installation

First, simply get a copy of the project:

```
$ git clone <repo url>
```

Create virtual environment

```
$ virtualenv -p /path/to/python venv
```

Activate the virtual environment:

```
$ source venv/bin/activate
```
Go to the project directory.
Install the project's requirements

```
$ pip install -r requirements.txt
```

Transform models so that we could deploy on a database

```
$ python manage.py makemigrations
```
**NOTE:** For migrations with custom name
```
python manage.py makemigrations --name <your_custom_name_for_migration> <app>
```

Synchronizes the database with the current set of models and migrations

```
$ python manage.py migrate
```

Now, simply make the development server automatically available on default host=```localhost``` and on default port=```8000```:

```
$ python manage.py runserver
```

Now, services should be up and running at the following host and port.

```
$ http://localhost:8000
```

---


# Test Application

---

1- To create first dummy user to perform actions. Run command:
```
$ python manage.py setup_first_user
```

This will create a user with:
```
    username = 'mahesh',
    first_name = 'Mahesh',
    last_name = 'Kumar',
    mobile_number = '9650550479',
    email = 'maheshjurel@gmail.com',
    password = 'test12345'
```

2- To generate authentication token for a user use below url.

    POST http://<HOSTNAME>/api-token-auth/

and in payload provide below details.

    {
        username: "mahesh",
        password: "test12345"
    }

3- To create more users:

    POST http://<HOSTNAME>/users/

and in payload provide below details (With jwt token in auth header).

    {
        "username": "mahesh2",
        "password": "test12345",
        "first_name": "Mahesh",
        "last_name": "Kumar",
        "mobile_number": "9650550479",
        "email": "2maheshjurel@gmail.com"
    }

4- To fetch details of user (With jwt token in auth header):

    GET http://<HOSTNAME>/users/<uuid of user>/


5- To run test cases:

```
$ pytest -v
```
