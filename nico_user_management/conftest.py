import pytest
from django.test import Client
from django.test.client import MULTIPART_CONTENT
from django_fakery import factory


def create_user(username, email):
    user = factory.m('users.User')(username=username, email=email)
    return user


def create_fake_user(username, email):
    """
    create_fake_user
        Method to create a fake user.
    :param email: username, email
    :return: User instance
    """
    user = create_user(username, email)
    return user


class CustomClient(Client):
    def get(self, path, data=None, follow=False, secure=False, **extra):
        return super().get(path, data, True, True, **extra)

    def post(self, path, data=None, content_type=MULTIPART_CONTENT,
             follow=False, secure=False, **extra):
        return super().post(path, data, content_type, True, True, **extra)

    def put(self, path, data='', content_type='application/octet-stream',
            follow=False, secure=False, **extra):
        return super().put(path, data, content_type, True, True, **extra)

    def patch(self, path, data='', content_type='application/octet-stream',
              follow=False, secure=False, **extra):
        return super().patch(path, data, content_type, True, True, **extra)

    def delete(self, path, data='', content_type='application/octet-stream',
               follow=False, secure=False, **extra):
        return super().delete(path, data, content_type, True, True, **extra)


@pytest.fixture()
def client(db):
    return CustomClient()

# ********* User Client ***************


@pytest.fixture()
def user_client(db):
    client = CustomClient()
    user = create_fake_user('john', 'john@test.com')
    client.force_login(user)
    client.user = user
    return client
