"""
.. module:: users.urls
   :synopsis: It contains API endpoints to perform CRUD operation on user entity.

.. moduleauthor:: Mahesh


"""
from django.conf.urls import url

import users.views as accounts_views

urlpatterns = [
    url(
        r'^(?P<pk>[a-f0-9-]+)/$',
        accounts_views.UserViewSet.as_view({
            'get': 'retrieve',
        }),
        name='user-details',
    ),
    url(
        r'^$',
        accounts_views.UserViewSet.as_view({
            'post': 'create',
        }),
        name='create-users',
    ),

]
