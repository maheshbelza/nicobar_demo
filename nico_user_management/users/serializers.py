"""
.. module:: users.serializers
   :synopsis: This file contains
        `BaseUserSerializer`,
        `CreateUserSerializer`,
        `UserSerializer`
     to create users and retrieve user details.

.. moduleauthor:: Mahesh


"""

from django.contrib.auth.hashers import make_password

from core import serializers as core_serializers

from .models import User


class BaseUserSerializer(core_serializers.BaseModelSerializer):
    """
        BaseUserSerializer
            Serializer defines how to validate User object which are going to save into database.
    """
    class Meta:
        model = User
        read_only_fields = (
            'id',
            'date_joined',
        )
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'date_joined',
            'mobile_number',
        )


class CreateUserSerializer(BaseUserSerializer):
    """
        CreateUserSerializer
            Serializer defines how to validate User object which are going to save into database.
    """
    class Meta:
        model = User
        extra_kwargs = {
            'password': {'write_only': True}
        }
        fields = BaseUserSerializer.Meta.fields + (
            'password',
        )

    def validate_email(self, value):
        value = value.lower()
        return value

    def validate_password(self, value):
        password_value = make_password(value)
        return password_value


class UserSerializer(BaseUserSerializer):
    """
        UserSerializer
            Serializer used to retrieve user details.
    """
    class Meta:
        model = User
        fields = read_only_fields = BaseUserSerializer.Meta.fields
