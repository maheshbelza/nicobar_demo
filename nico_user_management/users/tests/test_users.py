import pytest
from django_fakery import factory
from rest_framework import status as status_code

from core import test_resolve_urls as test_urls


def create_user(**kwargs):
    """
    create_user
        Method to create a user.
    :param kwargs: Keyword arguments
    :return: User objects
    """
    default_kwargs = {
        'email': 'usertest@test.com',
        'username': 'usertest',
    }
    default_kwargs.update(kwargs)
    return factory.m('users.User')(**default_kwargs)


@pytest.mark.django_db
class TestViewUserDetails(object):

    def get_user_data(self, **kwargs):
        user_data = {
            'email': 'jenny@test.com',
            'username': 'jenny',
            'first_name': 'Jenny',
            'last_name': 'Stiles',
            'mobile_number': '3332229999',
        }
        user_data.update(**kwargs)
        return user_data

    def test_authorised_users_can_retrieve_user_details(self, user_client):
        """
        test_authorised_users_can_retrieve_user_details
            Test authorized users can retrieve his/her details
        :param user_client : Client with a user logged in
        :return: HTTP_200_OK Expected
        """
        url_params = (user_client.user.id,)
        url = test_urls.get_user_details_url(url_params)
        response = user_client.get(url)

        assert response.status_code == status_code.HTTP_200_OK
        assert response.data.get('username') == user_client.user.username
        assert response.data.get('email') == user_client.user.email

    def test_authorised_users_cannot_retrieve_other_user_details(self, user_client):
        """
        test_authorised_users_cannot_retrieve_other_user_details
            Test authorized users cannot retrieve details of any other user
        :param user_client : Client with a user logged in
        :return: HTTP_404_NOT_FOUND Expected
        """
        jenny = create_user(**self.get_user_data())

        url_params = (jenny.id,)
        url = test_urls.get_user_details_url(url_params)
        response = user_client.get(url)

        assert response.status_code == status_code.HTTP_404_NOT_FOUND

    def test_unauthorised_users_cannot_retrieve_user_details(self, user_client):
        """
        test_unauthorised_users_cannot_retrieve_user_details
            Test unauthorized users cannot retrieve his/her details
        :param user_client : Client with a user logged in
        :return: HTTP_401_UNAUTHORIZED Expected
        """
        # Logging out the current user
        user_client.logout()
        url_params = (user_client.user.id,)
        url = test_urls.get_user_details_url(url_params)
        response = user_client.get(url)

        assert response.status_code == status_code.HTTP_401_UNAUTHORIZED

    def test_unauthorised_users_cannot_retrieve_other_user_details(self, user_client):
        """
        test_unauthorised_users_cannot_retrieve_other_user_details
            Test unauthorized users cannot retrieve other user details
        :param user_client : Client with a user logged in
        :return: HTTP_401_UNAUTHORIZED Expected
        """
        # Logging out the current user
        user_client.logout()
        jenny = create_user(**self.get_user_data())
        url_params = (jenny.id,)
        url = test_urls.get_user_details_url(url_params)
        response = user_client.get(url)

        assert response.status_code == status_code.HTTP_401_UNAUTHORIZED
