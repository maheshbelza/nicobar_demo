"""
.. module:: users.views
   :synopsis: This file contains `UserViewSet` to handle all the url requests
   for various operations on users.

.. moduleauthor:: Mahesh


"""
from core.views import BaseModelViewset
from users.models import User
from users.serializers import CreateUserSerializer, UserSerializer


class UserViewSet(BaseModelViewset):
    """
    UserViewSet
        This class combines the logic of CRUD operations for users. Only permitted users can
        perform respective operations.

        Inherits: BaseModelViewset
    """
    model = User

    permission_classes = (
    )

    serializer_class = UserSerializer
    action_serializers = {
        'create': CreateUserSerializer,
    }

    def apply_queryset_filters(self, queryset):
        # Filter queryset to let users view their own details only
        # Here it will throw 404 error if requesting user trying to access
        # any other user's details.

        # This can be done in a different way also, using permissions
        # Alternate way: Create a permission to check if requested user
        # want to access his/her own details or not, if yes, return True
        # and if no, return False from `has_object_permission` or `has_permission`
        queryset = queryset.filter(username=self.request.user.username)
        return queryset
