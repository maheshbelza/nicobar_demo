from django.core.management.base import BaseCommand
from django.db import transaction

from users.models import User


class Command(BaseCommand):

    def create_user(self, **kwargs):
        default_kwargs = {
        }
        default_kwargs.update(kwargs)
        raw_password = default_kwargs.pop('password', '')
        user, _ = User.objects.get_or_create(default_kwargs)
        user.set_password(raw_password)
        user.save()
        return user

    def handle(self, **options):
        # create user
        with transaction.atomic():
            self.create_user(
                username='mahesh',
                first_name='Mahesh',
                last_name='Kumar',
                mobile_number='9650550479',
                email='maheshjurel@gmail.com',
                password="test12345",
            )
