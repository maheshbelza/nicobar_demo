"""
.. module:: users.models
   :synopsis: This file contains `User` model to persist user details.

.. moduleauthor:: Mahesh


"""
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager
from django.db import models
from django.utils import timezone

from core.models import BaseModel


class User(BaseModel, AbstractBaseUser):
    """
        User
            This class define model used to store user entity details.
            Inherits : `BaseModel`, `AbstractBaseUser`
    """
    username = models.CharField(unique=True, max_length=100, verbose_name='Username')
    email = models.EmailField(unique=True, verbose_name='Email Address')
    first_name = models.CharField(max_length=100, verbose_name='First Name')
    last_name = models.CharField(max_length=100, verbose_name='Last Name')
    mobile_number = models.CharField(max_length=10, verbose_name='Mobile Number')
    date_joined = models.DateTimeField(default=timezone.now, verbose_name='Date Joined')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    objects = UserManager()
